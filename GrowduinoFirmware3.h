#pragma once

#define BH1750_QUIET true

#include "Arduino.h"
// #include "util.h"
#include <math.h>

#define MINVALUE -INFINITY


#define OUTPUTS 12
#define RELAY_START 36

#define LOGGERS 12

#define ANALOG_READ_AVG_TIMES 30
#define ANALOG_READ_AVG_DELAY 10
#define LIGHT_SENSOR_PIN_1 8
#define LIGHT_SENSOR_PIN_2 9

#define LIGHT_IN_SDA 22
#define LIGHT_IN_SCL 23
#define LIGHT_OUT_SDA 24
#define LIGHT_OUT_SCL 25

#define TH_SDA 26
#define TH_SCL 27
#define ONEWIRE_PIN 28
#define ONEWIRE_PIN2 29
#define USOUND_TRG 30
#define USOUND_ECHO 31
#define USND_RX 52
#define USND_TX 30
// #define SENSOR_SDA 29  // Y
// #define SENSOR_SCL 30  // B

#define EC_ENABLE 32
#define EC_DATA 33

#define PH_SDA 34
#define PH_SCL 35

#define CO2_RX 51
#define CO2_TX 50

#define PAR1_RX A9
#define PAR1_TX 22


//i2c pH sensor
#define I2C_MAX11647adr  0x36

#define USE_EC_SENSOR 1
#define EC_SAMPLE_TIMES 100
#define EC_CUTOFF 550
#define EC_TIMEOUT 10000

// when reading from ADC, values read higher than this are considered as error (CO2 sensor)
#define ADC_CUTOFF 1020
#define CO2_DATA 12

//LCD

#define LCD_BUFFER_LINES 12
#define LCD_DISPLAY_LINES 4
#define LCD_LINE_LEN 20

//#define DEBUG 1
#ifdef DEBUG
#define DEBUG_PH 1
#endif

//#include "outputs.h"
//#include "logger.h"
//#include "sensors.h"
//#include "ultrasound.h"
//#include "ds.h"
//#include "ups.h"
//#include "lcd.h"

unsigned long getSeconds();

int daymin(unsigned long seconds);
int daymin();

struct SensorConfig {
  int valuecheck;
  int co2_400;
  int co2_40k;
  int ph_4;
  int ph_7;
  int ec_low_ion;
  int ec_high_ion;
  int ec_offset;
  bool co2_probed;
  bool co2_digital;
};

