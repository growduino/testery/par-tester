#include "GrowduinoFirmware3.h"

/* all sensors that are not complex enough to have its own file */

#include <math.h>

#include <SoftwareWire.h>
#include <MHZ19.h>

#include <BH1750.h>

#include <SoftwareSerial.h>

// Not used, but needed by Adafruit_BME280.h
#include <SPI.h>

#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>

// SoftwareSerial CO2Serial(51, 50); // RX, TX
SoftwareSerial CO2Serial(CO2_RX, CO2_TX); // RX, TX

extern SensorConfig sensor_config;

SoftwareWire ph_soft_wire(PH_SDA, PH_SCL);
SoftwareWire light_in_soft_wire(LIGHT_IN_SDA, LIGHT_IN_SCL);
SoftwareWire light_out_soft_wire(LIGHT_OUT_SDA, LIGHT_OUT_SCL);
SoftwareWire th_soft_wire(TH_SDA, TH_SCL);

MHZ19 mhz(&CO2Serial);

BH1750 light_in;
BH1750 light_out;

bool th_status = false;

union ByteInt {
  int val;
  struct {
    // Access to the bytes of 'val'
    byte lo, hi;
  } bytes;
};

Adafruit_BME280 th_sensor;

float get_TH_temperature() {
  if (th_status) {
    float temp = th_sensor.readTemperature();
    if (temp < -140) {
      th_status = false;
      return MINVALUE;
    }
    return 10 * th_sensor.readTemperature();
  } else {
    return MINVALUE;
  }
}

float get_TH_humidity() {
  if (th_status) {
    return 10 * th_sensor.readHumidity();
  } else {
    return MINVALUE;
  }
}

void th_init() {
  if (not th_status) {
    th_soft_wire.begin();
    if (!th_status) {
      unsigned status = th_sensor.begin(0x76, &th_soft_wire);
      if (!status) {
        Serial.println("D: Could not find a valid BME280 sensor");
        th_status = false;
      } else {
        th_status = true;
      }
    }
  }
}

void doPhSetup() {
  ph_soft_wire.beginTransmission(I2C_MAX11647adr);
  ph_soft_wire.write(0b11010110);
  ph_soft_wire.write(0b00000010);
  ph_soft_wire.endTransmission();    // stop transmitting
}

float getPhMeasure() {
  int reading = 0;
  int reading2 = 0;

  ph_soft_wire.requestFrom(I2C_MAX11647adr, 2);
  if (2 <= ph_soft_wire.available()) { // if two bytes were received
    reading = ph_soft_wire.read();  // receive high byte (overwrites previous reading)
    reading2 = ph_soft_wire.read();
#ifdef DEBUG_PH
    Serial.print("D: pH hi ");
    Serial.println(reading, BIN);   // print the reading
    Serial.print("D: pH lo ");
    Serial.println(reading2, BIN);   // print the reading
#endif
    if ((reading & 0x02) == 0) {
      reading = reading & 0x03;
    }
    reading = reading << 8;    // shift high byte to be high 8 bits
    reading |= reading2;
#ifdef DEBUG_PH
    Serial.print("D: pH final ");
    Serial.println(reading);   // print the reading
#endif
    return (float) reading + 512; // return positive value
  } else {
#ifdef DEBUG_PH
    Serial.println("D: pH MINVALUE");
#endif
    return MINVALUE;
  }
}

float calibrate_ph(float raw_data) {

  float ph_4_val = 4;
  float ph_7_val = 7;
  float ph_4 = (float) sensor_config.ph_4;
  float ph_7 = (float) sensor_config.ph_7;
  if (isinf(raw_data)) {
    return MINVALUE;
  }
  float slope = (ph_7 - ph_4) / (ph_7_val - ph_4_val);
  float pH = ph_4_val + (((float) raw_data - ph_4) / slope);

  return pH;
}


void ec_enable() {
    /*
  pinMode(EC_DATA, INPUT);
  pinMode(EC_ENABLE, OUTPUT);
*/
  }

/* new (serial) version */
#define BUFLEN 32

char serial_buffer[BUFLEN + 1]; //+1 for \0
char char_read;
int serial_buffer_idx;
long sensor_value, sensor_value_triple;
bool ec_serial_started;

#include <SoftwareSerial.h>
SoftwareSerial ECSerial(53, 32);

float curr_to_ppfd(float current){
  float k = 2.558;
  float f = 0.97;
  float ppfd = (current / 96.6057) * k * f;
  return ppfd;
}


float getECMeasure_serial() {
  if (not ec_serial_started) {
    ECSerial.begin(19200);
    ec_serial_started = true;
  }
  ECSerial.listen();
  serial_buffer_idx = 0;
  delay(50);
  ECSerial.println("");
  ECSerial.print("m ");
  delay(1500);
  sensor_value = -1;
  sensor_value_triple = -1;
  if (ECSerial.available()) {
    while ((ECSerial.available()) && (serial_buffer_idx < BUFLEN)) {
      char_read = ECSerial.read();
      serial_buffer[serial_buffer_idx] = char_read;
      serial_buffer_idx++;
      delay(3);
    }
    serial_buffer[serial_buffer_idx] = 0;
    sscanf(serial_buffer, "%ld,%ld", &sensor_value, &sensor_value_triple);
    if ((sensor_value * 3 == sensor_value_triple) && sensor_value > 0) {
      return float(sensor_value / 10);
    }

  }
  return MINVALUE;
}

SoftwareSerial PARSerial(PAR1_RX, PAR1_TX);
bool par_serial_started;

float getPARMeasure_serial() {
  if (not par_serial_started) {
    PARSerial.begin(19200);
    par_serial_started = true;
  }
  PARSerial.listen();
  serial_buffer_idx = 0;
  PARSerial.print("m ");
  delay(500);
  sensor_value = -1;
  sensor_value_triple = -1;
  if (PARSerial.available()) {
    while ((PARSerial.available()) && (serial_buffer_idx < BUFLEN)) {
      char_read = PARSerial.read();
      serial_buffer[serial_buffer_idx] = char_read;
      serial_buffer_idx++;
      delay(3);
    }
    serial_buffer[serial_buffer_idx] = 0;
    Serial.print(">");
    Serial.println(serial_buffer);
    sscanf(serial_buffer, "%ld,%ld", &sensor_value, &sensor_value_triple);
    if (((sensor_value * 3 % 1000000) == sensor_value_triple) && sensor_value > 0) {
      return curr_to_ppfd(float(sensor_value));
    }

  }
  return MINVALUE;
}



float getECMeasure() {
    return getECMeasure_serial();
  long lowPulseTime_t = 0;
  long highPulseTime_t = 0;
  long lowPulseTime = 0;
  long highPulseTime = 0;
  float pulseTime;

  digitalWrite(EC_ENABLE, HIGH); // power up the sensor
  delay(100);
  for (unsigned int j = 0; j < EC_SAMPLE_TIMES; j++) {
    highPulseTime_t = pulseIn(EC_DATA, HIGH, EC_TIMEOUT);
    if (j == 0 and highPulseTime_t == 0)  {//abort on timeout on first read
      return MINVALUE;
    }

    highPulseTime += highPulseTime_t;
    if (highPulseTime_t == 0) {
      return MINVALUE;
    }
    lowPulseTime_t = pulseIn(EC_DATA, LOW, EC_TIMEOUT);
    lowPulseTime += lowPulseTime_t;

    if (lowPulseTime_t == 0) {
      return MINVALUE;
    }

  }

  lowPulseTime = lowPulseTime / EC_SAMPLE_TIMES;
  highPulseTime = highPulseTime / EC_SAMPLE_TIMES;

  pulseTime = (lowPulseTime + highPulseTime) / 2;

  digitalWrite(EC_ENABLE, LOW); // power down the sensor

  if (pulseTime >= EC_CUTOFF || pulseTime < 0) {
    return MINVALUE;
  }

  return pulseTime;
}

float calibrate_ec(float pulseTime) {
    return pulseTime;
  if (isinf(pulseTime)) {
    return MINVALUE;
  }
  float ec = MINVALUE;
  float ec_a, ec_b;

  float c_low = 1.278;
  float c_high = 4.523;
  float ec_high_ion = (float) (sensor_config.ec_high_ion + sensor_config.ec_offset);
  float ec_low_ion = (float) (sensor_config.ec_low_ion + sensor_config.ec_offset);

  ec_a =  (c_high - c_low) / (1 / ec_high_ion - 1 / ec_low_ion);
  ec_b = c_low - ec_a / (float) ec_low_ion;

  ec = 100 * (ec_a / (pulseTime + sensor_config.ec_offset) + ec_b);


#ifdef DEBUG_CALIB
  if (ec != MINVALUE) {
    Serial.print(F("EC pulse time: "));
    Serial.println(pulseTime);
  }
#endif
  return ec;
}

float getCO2DigitalMeasure() {
  CO2Serial.listen();
  MHZ19_RESULT response = mhz.retrieveData();
  if (response == MHZ19_RESULT_OK) {
    return (float) mhz.getCO2();
  } else {
    Serial.println("D: CO2 read failed");
    return MINVALUE;
  }
}

float getCO2Measure() {
  return getCO2DigitalMeasure();
}

void co2_init() {
  CO2Serial.begin(9600);
  MHZ19_RESULT response = mhz.retrieveData();
  if (response == MHZ19_RESULT_OK) {
    Serial.println("D: Disabling CO2 auto calibration");
    mhz.setAutoCalibration(false);
  }
}


float calibrate_co2(float raw_data) {
  return raw_data;
}

bool getCO2DigitalStatus() {
  MHZ19_RESULT response = mhz.retrieveData();
  return (response == MHZ19_RESULT_OK);
}

bool light_in_begin() {
  return light_in.begin(light_in_soft_wire);
}

bool light_out_begin() {
  return light_out.begin(light_out_soft_wire);
}

float light_in_read() {
  float lux = light_in.readLightLevel();
  if (lux < 0) {
    light_in_begin();
    return MINVALUE;
  } else {
    return lux;
  }
}

float light_out_read() {
  float lux =  light_out.readLightLevel();
  if (lux < 0) {
    light_out_begin();
    return MINVALUE;
  } else {
    return lux;
  }
}
